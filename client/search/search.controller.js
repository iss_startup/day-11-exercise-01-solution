(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {

        var vm = this;

        vm.searchForm = {
            dept_name: ""
        };

        vm.departments = [];

        vm.search = function () {
            // $http
            //     .get("/api/departments?dept_name=" + vm.searchForm.dept_name + "&someopte=" + "asdasd")
            $http
                .get("/api/departments", {
                    params: vm.searchForm
                })
                .then(function (response) {
                    console.log(response.data);
                    vm.departments = response.data;
                })
                .catch(function () {

                });


        };

        $http
            .get("/api/departments")
            .then(function (response) {
                console.log(response.data);
                vm.departments = response.data;
            })
            .catch(function () {

            });

    }
})();






