(function () {
    angular
        .module("DepartmentApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http"];

    function InsertCtrl($http) {
        var vm = this;

        vm.insertForm = {};

        vm.submit = function () {
            $http
                .post("/api/departments",vm.insertForm , {params: {hello: true}})
                .then(function () {

                })
                .catch(function () {

                });

        }
    }
})();






