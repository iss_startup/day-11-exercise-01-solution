//Create a model for dept_manager table
module.exports = function (sequelize, Sequelize) {
    var Manager = sequelize.define('manager', {
            emp_no: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            dept_no: {
                type: Sequelize.STRING
            },
            from_date: Sequelize.DATE,
            to_date: Sequelize.DATE
        }, {
            timestamps: false,
            tableName: "dept_manager"
        }
    );

    return Manager
};

