//Create a model for departments table
module.exports = function (conn, Sequelize) {
    var Department = conn.define("departments", {
        dept_no: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        dept_name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    return Department;
};

