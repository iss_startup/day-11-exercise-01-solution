//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

var Sequelize = require("sequelize");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = "igdefault";

var conn = new Sequelize('employees', MYSQL_USERNAME, MYSQL_PASSWORD, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});


var Department = require('./model/department')(conn, Sequelize);
var Manager = require('./model/manager')(conn, Sequelize);

//TODO : Create table relationships of manager and departments
Department.hasOne(Manager,{foreignKey : 'dept_no'});

// route or endpoint or api
    app.get("/api/departments", function (req, res) {
        var where = {};
        if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if(req.query.dept_no){
        where.dept_no = req.query.dept_no;
    }

    console.log(where);
    Department
        .findAll({
            where: where,
            include : [Manager]
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});

app.post("/api/departments", function (req, res) {
    console.log("Query", req.query);
    console.log("Body", req.body);

    Department
        .create({
            dept_no: req.body.deptNo,
            dept_name: req.body.deptName
        })
        .then(function (department) {
            res
                .status(200)
                .json(department);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json({error: true});
        })


})


//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});




























